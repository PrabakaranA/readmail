import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Properties;
import javax.mail.Address;
import javax.mail.BodyPart;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.NoSuchProviderException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.internet.MimeMessage;
import javax.mail.Flags.Flag;
import javax.mail.search.FlagTerm;

public class ReadMailSample {
	Properties properties = null;
	private Session session = null;
	private Store store = null;
	private Folder inbox = null;

	public ReadMailSample() {

	}

	public void readMails(String host, String port, String protocol, String userName, String password, String verificationPwd) {
		properties = new Properties();
		properties.setProperty("mail.host", host);
		properties.setProperty("mail.port", port);
		properties.setProperty("mail.transport.protocol", protocol);
		properties.setProperty("mail.debug", "true");

		session = Session.getInstance(properties,
				new javax.mail.Authenticator() {
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(userName, password);
					}
				});
		try {

			store = session.getStore(properties.getProperty("mail.transport.protocol"));
			store.connect(host, userName, verificationPwd);
			inbox = store.getFolder("INBOX");
			inbox.open(Folder.READ_ONLY);
			Message messages[] = inbox.search(new FlagTerm(
					new Flags(Flag.SEEN), false));
			System.out.println("Number of mails = " + messages.length);
			for (int i = 0; i < messages.length; i++) {
				Message message = messages[i];
				String msg = toRawString(message);
				Message ms = toMessage(session, msg);
				Address[] from = message.getFrom();
				System.out.println("-------------------------------");
				System.out.println("Date : " + message.getSentDate());
				System.out.println("From : " + from[0]); System.out.println("Subject: " +
				message.getSubject()); System.out.println("Content :");
				processMessageBody(message);
				System.out.println("--------------------------------");
			}
			inbox.close(true);
			store.close();
		} catch (NoSuchProviderException e) {
			e.printStackTrace();
		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}

	private Message toMessage(Session session2, String msg) {
		InputStream stream = new ByteArrayInputStream(msg.getBytes(StandardCharsets.UTF_8));
		MimeMessage message = null;
		try {
			message = new MimeMessage(session2, stream);
		} catch (MessagingException e) {
			e.printStackTrace();
		}
		return message;
	}

	private String toRawString(final Message msg){
		  if (msg != null) {
		    final ByteArrayOutputStream out = new ByteArrayOutputStream();
		    try {
				msg.writeTo(out);
				return out.toString(StandardCharsets.US_ASCII.toString()); //Raw message is always ASCII.
			} catch (Exception e) {
				e.printStackTrace();
			}
		  } else { 
		    return null; 
		  }
		return null;
		}

	public void processMessageBody(Message message) {
		try {
			Object content = message.getContent();
			if (content instanceof String) {
				System.out.println(content);
			} else if (content instanceof Multipart) {
				Multipart multiPart = (Multipart) content;
				procesMultiPart(multiPart);
			} else if (content instanceof InputStream) {
				InputStream inStream = (InputStream) content;
				int ch;
				while ((ch = inStream.read()) != -1) {
					System.out.write(ch);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}

	public void procesMultiPart(Multipart content) {
		try {
			int multiPartCount = content.getCount();
			for (int i = 0; i < multiPartCount; i++) {
				BodyPart bodyPart = content.getBodyPart(i);
				Object o;
				o = bodyPart.getContent();
				if (o instanceof String) {
					System.out.println(o);
				} else if (o instanceof Multipart) {
					procesMultiPart((Multipart) o);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		ReadMailSample sample = new ReadMailSample();
		sample.readMails(args[0], args[1], args[2], args[3], args[4], args[5]);
		//sample.readMails("imap.gmail.com", "143", "imap", "xxxxxx@xxxx.com", "yyyyyyyyy","zzzzzzzzzzzz");
	}
}
